<?php

namespace raitisg\passwordlength;

use Craft;
use craft\elements\User;
use yii\base\Event;
use yii\base\ModelEvent;

class Plugin extends \craft\base\Plugin
{
	public $hasCpSettings = true;

	public function init() {
		parent::init();

		Event::on(User::class, User::EVENT_BEFORE_VALIDATE, function (ModelEvent $event) {
			/** @var User $user */
			$user = $event->sender;

			if ($user->newPassword !== null) {
				$min_length = $this->getSettings()->minLength;

				if (mb_strlen($user->newPassword, 'utf8') < $min_length) {
					$user->addError('newPassword', sprintf(Craft::t('password-length', 'Minimal password length is %d symbols.'), $min_length));
					$event->isValid = false;
				}
			}
		});
	}

	protected function createSettingsModel() {
		return new \raitisg\passwordlength\models\Settings();
	}

	protected function settingsHtml() {
		return \Craft::$app->getView()->renderTemplate('password-length/settings', [
			'settings' => $this->getSettings()
		]);
	}
}
