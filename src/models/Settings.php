<?php

namespace raitisg\passwordlength\models;

use craft\base\Model;

class Settings extends Model
{
	/**
	 * @var int
	 */
	public $minLength = 6;

	public function rules() {
		return [
			[['minLength'], 'required'],
		];
	}
}
